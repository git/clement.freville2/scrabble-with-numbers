mod app;
mod hand_view;
mod remote_view;
mod tile_view;
mod types;

use app::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
