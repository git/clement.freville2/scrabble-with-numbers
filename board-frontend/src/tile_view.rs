use board_shared::tile::Tile;
use yew::html;
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct PlacedTileViewProps {
    pub x: usize,
    pub y: usize,
    pub tile: Option<Tile>,
    pub on_click: Callback<(usize, usize)>,
}

#[function_component(PlacedTileView)]
pub fn placed_tile_view(
    PlacedTileViewProps {
        x,
        y,
        tile,
        on_click,
    }: &PlacedTileViewProps,
) -> Html {
    let x = *x;
    let y = *y;
    let on_select = {
        let on_click = on_click.clone();
        Callback::from(move |_| on_click.emit((x, y)))
    };
    html! {
        <td class="cell" key={y} onclick={on_select}>{ tile.map(|tile| {
            html! { tile }
        }).unwrap_or_else(|| {
            html! { "" }
        })}</td>
    }
}

#[derive(Properties, PartialEq)]
pub struct TileViewProps {
    pub tile: Tile,
    pub on_select: Callback<usize>,
    pub idx: usize,
}

#[function_component(TileView)]
pub fn tile_view(
    TileViewProps {
        tile,
        on_select,
        idx,
    }: &TileViewProps,
) -> Html {
    let on_select = on_select.clone();
    let idx = *idx;
    html! {
        <div class="tile" onclick={Callback::from(move |_| {
            on_select.emit(idx)
        })}>{ tile.to_string() }</div>
    }
}
