pub enum SelectedTile {
    InHand(usize),
    Equals,
    None,
}
