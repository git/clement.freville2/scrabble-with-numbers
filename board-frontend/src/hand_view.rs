use crate::tile_view::TileView;
use board_shared::game::Hand;
use yew::html;
use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct HandViewProps {
    pub hand: Hand,
    pub on_select: Callback<usize>,
}

#[function_component(HandView)]
pub fn hand_view(HandViewProps { hand, on_select }: &HandViewProps) -> Html {
    let on_select = on_select.clone();
    html! {
        <div class="hand">
            { hand.tiles.iter().enumerate().map(|(i, tile)| html! {
                <TileView tile={*tile} key={i} idx={i} on_select={on_select.clone()} />
            }).collect::<Html>() }
        </div>
    }
}
