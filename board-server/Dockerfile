FROM rust:1.77.0-slim as builder

WORKDIR /usr/src/board

# Build with musl to statically link
RUN rustup target add x86_64-unknown-linux-musl

COPY Cargo.toml /usr/src/board/Cargo.toml
COPY board-frontend /usr/src/board/board-frontend
COPY board-network /usr/src/board/board-network
COPY board-server /usr/src/board/board-server
COPY board-shared /usr/src/board/board-shared

RUN cargo build --target x86_64-unknown-linux-musl --release --bin board-server

FROM scratch AS runtime

COPY --from=builder /usr/src/board/target/x86_64-unknown-linux-musl/release/board-server /

EXPOSE 8080

ENTRYPOINT ["/board-server"]
