# WebSocket server implementation

This crate provides a WebSocket server for the Scrabble with numbers game.

## Build

This project uses [Cargo](https://crates.io/), so ensure you have it installed.

```bash
cargo build --release
```

## Usage

The server listens on port `8080` by default. You can change this by specifying a program argument:

```bash
# Run a debug build on port 1234
cargo run -- '0.0.0.0:1234'
# Run an already built binary on the default port
./board-server
```

## Protocol

The server only understands certain predefined messages.
All messages are sent as JSON strings are can only be sent by either the client or the server.

You can see the exact layout of the messages in the [protocol file](../board-network/src/protocol.rs).

Messages sent and received shouldn't contain any unnecessary indentation.

## Sample client

```js
// Create WebSocket connection.
const socket = new WebSocket('ws://localhost:8080');

// Connection opened
socket.addEventListener('open', (event) => {
    // Create a new room, and join it it immediately with the player name "player_name"
    // The server will respond with a JoinedRoom message which contains the room name
    socket.send(JSON.stringify({ CreateRoom: 'player_name' }));
});

// Listen for messages
socket.addEventListener('message', (event) => {
    console.log('Message from server', JSON.parse(event.data));
});
```

## Run as Linux service

```
[Unit]
Description=Scrabble with numbers - WebSocket Server
After=network.target

[Service]
ExecStart=board-server '0.0.0.0:21110'

[Install]
WantedBy=multi-user.target
```
