use board_network::protocol::ServerMessage;
use board_shared::game::Hand;
use futures::channel::mpsc::UnboundedSender;

#[derive(Debug)]
pub struct Player {
    pub name: String,
    pub score: u32,
    pub hand: Hand,
    pub ws: Option<UnboundedSender<ServerMessage>>,
}
