use async_trait::async_trait;
use redis::{AsyncCommands, RedisError};
use std::env;
use std::sync::Arc;

const LEADERBOARD: &str = "leaderboard";
const LEADERBOARD_SIZE: isize = 10;
type LeaderboardEntry = (String, u32);

#[async_trait]
pub trait Leaderboard: Send + Sync {
    async fn add_score(&self, player_name: &str, score: u32) -> Result<(), RedisError>;
    async fn get_highscores(&self) -> Result<Vec<LeaderboardEntry>, RedisError>;
    fn driver(&self) -> &'static str {
        "in-memory"
    }
}

struct RedisLeaderboard {
    client: redis::Client,
}

impl RedisLeaderboard {
    fn new(client: redis::Client) -> Self {
        Self { client }
    }
}

#[async_trait]
impl Leaderboard for RedisLeaderboard {
    async fn add_score(&self, player_name: &str, score: u32) -> Result<(), RedisError> {
        let mut con = self.client.get_multiplexed_async_connection().await?;
        con.zadd(LEADERBOARD, player_name, score).await?;
        Ok(())
    }

    async fn get_highscores(&self) -> Result<Vec<LeaderboardEntry>, RedisError> {
        let mut con = self.client.get_multiplexed_async_connection().await?;
        let count: isize = con.zcard(LEADERBOARD).await?;
        let leaderboard: Vec<LeaderboardEntry> = con
            .zrange_withscores(LEADERBOARD, 0, (count - 1).min(LEADERBOARD_SIZE))
            .await?;
        Ok(leaderboard)
    }

    fn driver(&self) -> &'static str {
        "redis"
    }
}

#[derive(Debug, Default)]
pub struct InMemoryLeaderboard();

#[async_trait]
impl Leaderboard for InMemoryLeaderboard {
    async fn add_score(&self, _: &str, _: u32) -> Result<(), RedisError> {
        Ok(())
    }

    async fn get_highscores(&self) -> Result<Vec<LeaderboardEntry>, RedisError> {
        Ok(Vec::new())
    }
}

pub fn provide_leaderboard() -> Arc<dyn Leaderboard> {
    match env::var("REDIS_HOST") {
        Ok(redis_host_name) => match redis::Client::open(format!("redis://{redis_host_name}/")) {
            Ok(client) => Arc::new(RedisLeaderboard::new(client)),
            Err(_) => Arc::new(InMemoryLeaderboard::default()),
        },
        Err(_) => Arc::new(InMemoryLeaderboard::default()),
    }
}
