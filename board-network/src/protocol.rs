use board_shared::board::Board;
use board_shared::position::Position2d;
use board_shared::tile::Tile;
use serde::{Deserialize, Serialize};

/// A message sent by the client to the server.
#[derive(Debug, Deserialize, Serialize)]
pub enum ClientMessage {
    /// Creates a new room and join it with the given player name.
    ///
    /// The server answers with a JoinedRoom message.
    CreateRoom(String),
    /// Join an existing room with the given room name and player name.
    ///
    /// The server answers with a JoinedRoom message.
    JoinRoom(String, String),
    /// Notify that the client has temporary left the game.
    Disconnected,
    /// Start the game if the client has the permission.
    StartGame,
    /// Try to place a tile from the hand on the board.
    ///
    /// The server will validate the move and answer with a TilePlaced if the message is valid.
    TileUse(Position2d, usize),
    /// Try to place an equal sign on the board.
    TilePlaceEqual(Position2d),
    /// Try to remove a tile from the board to add it to the hand.
    TileTake(Position2d),
    /// Get the server to validate the current player moves.
    Validate,
}

/// A message sent by the server to the client.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ServerMessage {
    /// Informs that a room has been joined.
    JoinedRoom {
        room_name: String,
        players: Vec<(String, u32, bool)>,
        board: Board,
        active_player: usize,
        has_started: bool,
    },
    /// Notify that the room cannot be joined.
    JoinFailed(String),
    /// Notify that new player has joined the game.
    PlayerConnected(String),
    /// Notify that new player has rejoined the game.
    PlayerReconnected(usize),
    /// Notify that new player has temporary left the game.
    PlayerDisconnected(usize),
    /// Change the current player
    PlayerTurn(usize),
    /// Update the current hand of the player
    SyncHand(Vec<Tile>),
    /// Update the score of the n-th player.
    SyncScore(usize, u32),
    /// Informs that a tile has been placed
    TilePlaced(Position2d, Tile),
    /// Informs that a tile has been removed
    TileRemoved(Position2d),
    TurnRejected(String),
    /// Notify that the game has ended.
    GameOver,
}
