//! # Scrabble with Numbers
//!
//! This crate provides the core game logic for the Scrabble with Numbers game.
//! It can be used standalone, or as a library for other projects.
//!
//! ## Features
//! - Create and verify game expressions, such as `2*3+4*5`.
//! - Generate valid automatic moves for a given board state, with the `ai` feature.
//! - Check if a player move valid.
//! - Calculate the score of an expression.
//!
//! If you are looking for a server implementation, see the `board-server` crate.

#[cfg(feature = "ai")]
pub mod ai;
pub mod board;
pub mod deck;
pub mod expr;
pub mod game;
mod lexer;
mod parser;
pub mod position;
pub mod score;
pub mod tile;
