use crate::tile::{Operator, Tile};
use std::fmt;

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    NumberLiteral(i64),
    Operator(Operator),
    LeftParen,
    RightParen,
    Equals,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum DecimalToken {
    NumberLiteral(f64),
    Operator(Operator),
    LeftParen,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Token::NumberLiteral(value) => write!(f, "{value}"),
            Token::Operator(operator) => write!(f, "{operator}"),
            Token::LeftParen => write!(f, "("),
            Token::RightParen => write!(f, ")"),
            Token::Equals => write!(f, "="),
        }
    }
}

/// Tokenize a sequence of tiles into tokens.
pub fn lexer(input: &[Tile]) -> Vec<Token> {
    let mut result = Vec::new();
    lexer_reuse(input, &mut result);
    result
}

/// Tokenize a sequence of tiles into tokens.
pub fn lexer_reuse(input: &[Tile], result: &mut Vec<Token>) {
    let mut it = input.iter().peekable();
    while let Some(&c) = it.peek() {
        match c {
            Tile::Digit(digit) => {
                let mut has_right_parenthesis = digit.has_right_parenthesis;
                if digit.has_left_parenthesis {
                    result.push(Token::LeftParen);
                }
                let mut value = digit.value as i64;
                it.next();
                while let Some(&Tile::Digit(digit)) = it.peek() {
                    if digit.has_left_parenthesis {
                        break;
                    }
                    value = value * 10 + digit.value as i64;
                    it.next();
                    if digit.has_right_parenthesis {
                        has_right_parenthesis = true;
                        break;
                    }
                }
                result.push(Token::NumberLiteral(value));
                if has_right_parenthesis {
                    result.push(Token::RightParen);
                }
            }
            Tile::Operator(operator) => {
                result.push(Token::Operator(*operator));
                it.next();
            }
            Tile::Equals => {
                result.push(Token::Equals);
                it.next();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tile::Digit;

    #[test]
    fn test_lex() {
        let input = [
            Tile::Digit(Digit::new(1)),
            Tile::Digit(Digit::new(2)),
            Tile::Digit(Digit::new(3)),
            Tile::Operator(Operator::Add),
            Tile::Digit(Digit::new(4)),
            Tile::Digit(Digit::new(5)),
            Tile::Digit(Digit::new(6)),
            Tile::Equals,
        ];
        let expected = vec![
            Token::NumberLiteral(123),
            Token::Operator(Operator::Add),
            Token::NumberLiteral(456),
            Token::Equals,
        ];
        assert_eq!(lexer(&input), expected);
    }

    #[test]
    fn test_lex_parentheses() {
        let input = [
            Tile::Digit(Digit::new(1)),
            Tile::Operator(Operator::Subtract),
            Tile::Digit(Digit {
                value: 2,
                has_left_parenthesis: true,
                has_right_parenthesis: false,
            }),
            Tile::Operator(Operator::Add),
            Tile::Digit(Digit {
                value: 3,
                has_left_parenthesis: false,
                has_right_parenthesis: true,
            }),
        ];
        let expected = vec![
            Token::NumberLiteral(1),
            Token::Operator(Operator::Subtract),
            Token::LeftParen,
            Token::NumberLiteral(2),
            Token::Operator(Operator::Add),
            Token::NumberLiteral(3),
            Token::RightParen,
        ];
        assert_eq!(lexer(&input), expected);
    }

    #[test]
    fn test_lex_parentheses_long() {
        let input = [
            Tile::Digit(Digit {
                value: 1,
                has_left_parenthesis: true,
                has_right_parenthesis: false,
            }),
            Tile::Digit(Digit::new(2)),
            Tile::Digit(Digit::new(3)),
            Tile::Digit(Digit {
                value: 4,
                has_left_parenthesis: false,
                has_right_parenthesis: true,
            }),
        ];
        let expected = vec![
            Token::LeftParen,
            Token::NumberLiteral(1234),
            Token::RightParen,
        ];
        assert_eq!(lexer(&input), expected);
    }
}
